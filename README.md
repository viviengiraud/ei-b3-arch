# EI
## Première étape
* Lorsqu'un utilisateur arrive à la racine de votre site, si celui-ci n'est pas connecté, redirigez-le vers la route lui permettant de se connecter ;
* Si celui-ci est déjà connecté, redirigez-le vers sa page utilisateur.

## Deuxième étape
* Faites en sorte que lorsque l'on accède à la route /users/ celle-ci vous retourne la liste des emails des utilisateurs enregistrés.

## Troisième étape
* Ajoutez une route dynamique /dyn/ permettant d'afficher l'élèment dynamique.


## Questions
Repondez aux questions du fichier questions.md

## Nota Bene
* Si ceux-ci sont nécessaires, utilisez les bons codes HTTP

## À la fin de votre EI
* Retournez, zippé au format B3-NOM-EI20210429.zip, les fichiers du dossier EI, avec vos modifictions.