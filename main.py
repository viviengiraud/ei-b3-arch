# -*- coding: utf-8 -*-
import bottle
import sqlite3
import random


def generate_cookie_value():
    return str(''.join(random.choice('0123456789ABCDEF') for i in range(32)))


@bottle.route('/login', method=['GET', 'POST'])
def login():
    if bottle.request.method == 'GET':
        return bottle.template('login_template')
    else:
        username = bottle.request.forms.username
        password = bottle.request.forms.password

        if username == "":
            return bottle.HTTPResponse({"error": True, "message": "Il manque le nom d'utilisateur"}, status=400)

        if password == "":
            return bottle.HTTPResponse({"error": True, "message": "Il manque le mot de passe"}, status=400)

        conn = sqlite3.connect('facebook.db')
        conn.set_trace_callback(print)

        c = conn.cursor()

        c.execute("SELECT * FROM facebook WHERE username = ?", (username,))

        result = c.fetchone()
        if result is None:
            return bottle.HTTPResponse({"error": True, "message": "L'utilisateur n'existe pas"}, status=404)

        db_password = result[3]
        email       = result[2]

        if password == db_password:
            cookie = generate_cookie_value()

            conn = sqlite3.connect('facebook.db')
            c = conn.cursor()
            c.execute("UPDATE facebook SET cookie = ? WHERE id = ?", (cookie, result[0]))
            conn.commit()

            bottle.response.set_cookie("fb_id", cookie)
            return {"error": False, "message": f"Bienvenue, votre adresse mail est {email}"}
        else:
            return {"error": True, "message": "Les credentials ne sont pas bons"}


@bottle.route('/signup', method=['GET', 'POST'])
def signup():
    if bottle.request.method == 'GET':
        return bottle.template('signup_template')
    else:
        username = bottle.request.forms.username
        password = bottle.request.forms.password
        email    = bottle.request.forms.email

        if username == "":
            return {"error": True, "message": "Il manque le nom d'utilisateur"}

        if password == "":
            return {"error": True, "message": "Il manque le mot de passe"}

        if email == "":
            return {"error": True, "message": "Il manque l'adresse email'"}

        conn = sqlite3.connect('facebook.db')
        c = conn.cursor()
        c.execute("INSERT INTO facebook (username,password,email) VALUES (?, ?, ?)", (username, password, email))
        conn.commit()

        return {"error": False, "message": f"Successfully signed up as {username} id: {c.lastrowid}"}


@bottle.route('/users/<user_id>', method='GET')
def get_user_info(user_id=None):
    conn = sqlite3.connect('facebook.db')
    c = conn.cursor()

    # /users/{id}
    c.execute('SELECT * FROM facebook WHERE id = ?', (user_id,))
    result = c.fetchone()

    if result is None:
        # Only use HTTPResponse here because we want to return 404 HTTP code instead of default one (200)
        return bottle.HTTPResponse({"error": True, "message": f"User {user_id} does not exists"}, status=404)

    user_cookie = result[4]
    if user_cookie == bottle.request.get_cookie("fb_id"):
        return {"error": False, "user": {"id": result[0], "name": result[1], "email": result[2], "cookie": result[4]}}
    else:
        return {"error": False, "user": {"id": result[0], "name": result[1]}}


@bottle.route('/user', method='GET')
def user():
    # Retrieve session cookie
    fb_id = bottle.request.get_cookie("fb_id")

    # Check if it's on the DB
    conn = sqlite3.connect('facebook.db')
    c = conn.cursor()
    c.execute('SELECT * FROM facebook WHERE cookie = ?', (fb_id,))
    result = c.fetchone()

    if result is None:
        return {"error": True}

    return bottle.template('user_info', username=result[1], email=result[2])


bottle.run(host="127.0.0.1", port=8080, reloader=True)
